## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

- Code Review and Show Case.

- Learn the Integration Test and Practice Integration Test.

- Learn SpringBoot 3 layers and Test Pyramid.

- Do SpringBoot 3 layers Practice

- Learn about Controller Advice.

## R (Reflective): Please use one word to express your feelings about today's class.

   Hard.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

- In this afternoon's programming exercise, I was stuck at one point because I didn't know enough about integration testing and Mock apis, and I wasn't familiar with Mock.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

- It is necessary to learn SpringBoot 3 layers, which separates the business code from the database operation code, so that I can better understand the role of the three layers.
- Follow Restful style.
