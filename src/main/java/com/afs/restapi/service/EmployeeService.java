package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeSalaryUnMatchedException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee insert(Employee employee) {
        if (!employee.isAgeMatchRule()){
            throw new AgeErrorException();
        }
        if (!employee.isAgeSalaryMatchRule()){
            throw new AgeSalaryUnMatchedException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }

    public Employee update(int id, Employee employee) {
        if (employeeRepository.findById(id) == null){
            throw new EmployeeNotFoundException();
        }
        return employeeRepository.update(id, employee);
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findById(id);
        Employee updateEmployee = new Employee();
        BeanUtils.copyProperties(employee, updateEmployee);
        updateEmployee.setStatus(false);
        employeeRepository.update(id, updateEmployee);
    }
}
