package com.afs.restapi.config;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeSalaryUnMatchedException;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionConfig {

    @ExceptionHandler({EmployeeNotFoundException.class, CompanyNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseData handleNotFoundException(Exception e){
        return new ResponseData(HttpStatus.NOT_FOUND.value(), e.getMessage());
    }

    @ExceptionHandler({AgeErrorException.class, AgeSalaryUnMatchedException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseData handleParamsException(Exception e){
        return new ResponseData(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }
}
