package com.afs.restapi.exception;

public class AgeErrorException extends RuntimeException{
    public AgeErrorException(){
        super("age should belong to 18 to 65");
    }
}
