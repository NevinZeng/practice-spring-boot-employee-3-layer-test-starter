package com.afs.restapi.exception;

public class AgeSalaryUnMatchedException extends RuntimeException{
    public AgeSalaryUnMatchedException(){
        super("age salary un match");
    }
}
