package com.afs.restapi.repository;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {

    private static final List<Company> companies = new ArrayList<>();

    public CompanyRepository() {
        List<Employee> employeesInSpring = new ArrayList<>();
        employeesInSpring.add(new Employee(1, "alice", 21, "female", 6000));
        employeesInSpring.add(new Employee(2, "bob", 20, "male", 6200));
        employeesInSpring.add(new Employee(3, "charles", 22, "mfale", 5800));

        List<Employee> employeesInBoot = new ArrayList<>();
        employeesInBoot.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInBoot.add(new Employee(2, "ethan", 19, "male", 6000));

        companies.add(new Company(1, "spring", employeesInSpring));
        companies.add(new Company(2, "boot", employeesInBoot));
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void addCompany(Company company) {
        companies.add(new Company(generateNewId(), company.getCompanyName(), company.getEmployees()));
    }

    public Company findById(int id) {
        return companies.stream().filter(company -> company.getId() == id).findFirst().orElseThrow(CompanyNotFoundException::new);
    }

    private int generateNewId() {
        int maxId = companies.stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }

    public void clearAll() {
        companies.clear();
    }


    public void delete(Company companyToDelete) {
        companies.remove(companyToDelete);
    }

    public List<Company> getCompaniesByPagination(Integer pageIndex, Integer pageSize) {
        return companies.stream()
                .skip((long) (pageIndex - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeesByCompanyId(Integer companyId) {
        return companies.stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .map(Company::getEmployees)
                .orElse(Collections.emptyList());
    }

    public Company getCompanyById(Integer companyId) {
        return companies.stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .orElse(null);
    }
}

