package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.AgeSalaryUnMatchedException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class EmployeeServiceTest {

    private final EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private final EmployeeService employeeService = new EmployeeService(employeeRepository);


    @Test
    void should_throw_age_error_exception_and_do_not_call_repository_when_perform_insert_given_two_employee_with_age_15_and_66() {
        // given
        Employee employeeYoung = new Employee(1, "Nevin", 15, "Male", 1800);
        Employee employeeOld = new Employee(2, "Zeng", 66, "Male", 1800);

        // when  then
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insert(employeeYoung));
        Assertions.assertThrows(AgeErrorException.class, () -> employeeService.insert(employeeOld));
        verify(employeeRepository, times(0)).insert(any());
        verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    void should_throw_age_salary_un_matched_exception_and_do_not_call_repository_when_perform_insert_given_employee_with_age_35_and_salary_10000() {
        // given
        Employee employee = new Employee(1, "Nevin", 35, "Male", 10000);

        // when  then
        Assertions.assertThrows(AgeSalaryUnMatchedException.class, () -> employeeService.insert(employee));
        verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    void should_return_employee_with_active_true_when_perform_insert_given_employee_matched_rule() {
        // given
        Employee employee = new Employee(1, "Nevin", 25, "Male", 10000);
        Employee employeeToReturn = new Employee(1, "Nevin", 25, "Male", 10000);
        employeeToReturn.setStatus(true);

        when(employeeRepository.insert(any())).thenReturn(employeeToReturn);

        // when
        Employee employeeSaved = employeeService.insert(employee);

        // then
        Assertions.assertTrue(employeeSaved.getStatus());
        verify(employeeRepository).insert(argThat(employeeToSave -> {
            Assertions.assertTrue(employeeToSave.getStatus());
            return true;
        }));
    }

    @Test
    void should_set_status_false_and_call_repository_when_perform_delete_given_employee_id() {
        // given
        Employee employee = new Employee(1, "Nevin", 25, "Male", 10000);
        employee.setStatus(true);


        when(employeeRepository.findById(anyInt())).thenReturn(employee);
        employeeService.delete(1);

        // then
        when(employeeRepository.findById(anyInt())).thenReturn(null);
        Employee findEmployee = employeeService.findById(1);
        Assertions.assertNull(findEmployee);

        verify(employeeRepository).update(anyInt(),argThat(employeeToSave -> {
            Assertions.assertFalse(employeeToSave.getStatus());
            return true;
        }));
    }

    @Test
    void should_throw_not_found_exception_and_do_not_call_repository_when_perform_update_given_employee_with_status_false() {
        // given
        Employee employee = new Employee(1, "Nevin", 25, "Male", 10000);
        employee.setStatus(true);
        Employee employeeToUpdate = new Employee(1, "Nevin", 25, "Male", 20000);
        // when
        when(employeeRepository.findById(anyInt())).thenReturn(employee);

        when(employeeRepository.findById(anyInt())).thenReturn(null);

        // then
        Assertions.assertThrows(EmployeeNotFoundException.class, () -> employeeService.update(1, employeeToUpdate));

        verify(employeeRepository, times(0)).update(anyInt(), any());

    }
}