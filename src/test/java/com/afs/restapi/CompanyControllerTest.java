package com.afs.restapi;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {
        //given
        Company company = buildCompanyMZ();
        companyRepository.addCompany(company);

        //when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Company> companies = companyRepository.getCompanies();

        assertEquals(1, companies.size());
        Company companyGet = companies.get(0);
        assertEquals(1, companyGet.getId());
        assertEquals("MZ", companyGet.getCompanyName());
    }

    private static Company buildCompanyMZ() {
        Company company = new Company(1, "MZ",
                List.of(new Employee(1, "Susan", 22, "Female", 10000),
                        new Employee(2, "Zhangsan", 22, "Male", 10000)));
        return company;
    }

    private static Company buildCompanyCS() {
        Company company = new Company(2, "CS",
                List.of(new Employee(3, "Wangwu", 22, "Female", 10000),
                        new Employee(4, "Liliu", 22, "Male", 10000)));
        return company;
    }

    private static Company buildCompanyXM() {
        Company company = new Company(3, "XM",
                List.of(new Employee(5, "HAHA", 22, "Female", 10000),
                        new Employee(6, "XIXI", 22, "Male", 10000)));
        return company;
    }

    @Test
    void should_return_company_mz_with_id_1_when_perform_get_by_id_given_companies_in_repo() throws Exception {
        //given
        Company company1 = buildCompanyMZ();
        Company company2 = buildCompanyCS();
        companyRepository.addCompany(company1);
        companyRepository.addCompany(company2);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(company1)));
    }

    @Test
    void should_return_one_page_companies_when_perform_get_by_page_given_companies_in_repo() throws Exception {

        //given
        Company company1 = buildCompanyMZ();
        Company company2 = buildCompanyCS();
        Company company3 = buildCompanyXM();
        companyRepository.addCompany(company1);
        companyRepository.addCompany(company2);
        companyRepository.addCompany(company3);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies?pageIndex=1&pageSize=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(List.of(company1, company2))));
    }

    @Test
    void should_return_company_save_with_id_when_perform_post_given_a_company() throws Exception {
        //given
        Company company = buildCompanyMZ();
        String susanJson = mapper.writeValueAsString(company);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isCreated());
        Company companySaved = companyRepository.findById(1);
        assertEquals(company.getId(), companySaved.getId());
        assertEquals(company.getCompanyName(), companySaved.getCompanyName());
    }

    @Test
    void should_update_company_in_repo_when_perform_put_by_id_given_company_in_repo_and_update_info() throws Exception {

        //given
        Company companyMZ = buildCompanyMZ();
        companyRepository.addCompany(companyMZ);
        Company toBeUpdateCompanyCS = buildCompanyMZ();
        toBeUpdateCompanyCS.setCompanyName("CSS");
        String companyJson = mapper.writeValueAsString(toBeUpdateCompanyCS);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyJson))
                .andExpect(status().isOk())
                .andExpect(content().json(companyJson));
        Company companyInRepo = companyRepository.findById(1);
        assertEquals(toBeUpdateCompanyCS.getCompanyName(), companyInRepo.getCompanyName());
    }

    @Test
    void should_del_company_in_repo_when_perform_del_by_id_given_companies() throws Exception {
        //given
        Company company = buildCompanyMZ();
        companyRepository.addCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1))
                .andExpect(status().isNoContent());

        //then
        assertThrows(CompanyNotFoundException.class, () -> companyRepository.findById(1));
    }

    @Test
    void should_return_employee_susan_zhangsan_in_companyMZ_when_perform_get_employee_by_company_id_given_companies() throws Exception {
        //given
        Company company = buildCompanyMZ();
        companyRepository.addCompany(company);

        List<Employee> employees = List.of(new Employee(1, "Susan", 22, "Female", 10000),
                new Employee(2, "Zhangsan", 22, "Male", 10000));
        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/{companyId}/employees", 1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(mapper.writeValueAsString(employees)));
    }
}